//
//  SCH_DRAW_PANEL_GAL.cpp
//  kicad
//
//  Created by Eugene Stewart on 1/26/16.
//
//

#include "SCH_DRAW_PANEL_GAL.hpp"
#include <worksheet_viewitem.h>

#include "sch_sheet.h"

SCH_DRAW_PANEL_GAL::SCH_DRAW_PANEL_GAL( wxWindow* pParentWindow, wxWindowID aWindowId, const wxPoint& aPosition, const wxSize& aSize, GAL_TYPE aGalType = GAL_TYPE_OPENGL )
: EDA_DRAW_PANEL_GAL(pParentWindow, aWindowId, aPosition, aSize, aGalType)
{
    
}

SCH_DRAW_PANEL_GAL::~SCH_DRAW_PANEL_GAL()
{
    if (m_worksheet){
        delete m_worksheet;
        m_worksheet = nullptr;
    }

}

void
SCH_DRAW_PANEL_GAL::DisplaySheet(const SCH_SHEET *sheet)
{
    m_view->Clear();
    
    
    
    return;
}
