//
//  SCH_DRAW_PANEL_GAL.hpp
//  kicad
//
//  Created by Eugene Stewart on 1/26/16.
//
//

#ifndef SCH_DRAW_PANEL_GAL_hpp
#define SCH_DRAW_PANEL_GAL_hpp

#include <class_draw_panel_gal.h>

namespace KIGFX
{
    class WORKSHEET_VIEWITEM;
    class RATSNEST_VIEWITEM;
}
class COLORS_DESIGN_SETTINGS;
class SCH_SHEET;

/**
 *  Canvas for draw electronic Schema
 */
class SCH_DRAW_PANEL_GAL : public EDA_DRAW_PANEL_GAL
{
public:
    
    SCH_DRAW_PANEL_GAL( wxWindow* pParentWindow, wxWindowID aWindowId, const wxPoint& aPosition, const wxSize& aSize, GAL_TYPE aGalType);
    virtual ~SCH_DRAW_PANEL_GAL();

    void DisplaySheet( const SCH_SHEET* sheet );
    
    void SetWorkSheet( KIGFX::WORKSHEET_VIEWITEM* aWorkSheet );
    
    void UserColorScheme( const COLORS_DESIGN_SETTINGS* pSettings );
    
//    virtual void SetHighContrastLayer( LAYER_ID aLayer );
//    virtual void SetTopLayer( LAYER_ID aLayer );
    
protected:
    
    KIGFX::WORKSHEET_VIEWITEM*  m_worksheet;
//    KIGFX::RATSNEST_VIEWITEM*   m_ratsnest;
};

#endif /* SCH_DRAW_PANEL_GAL_hpp */
