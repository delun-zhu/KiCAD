KiCAD  
------------

著名的kicad项目，官网上的版本在Mac OSX上运行有bug，这个fork是专门正对MacOSX版本进行完善。 目前正在完善中。    


Mac OS X环境下编译KiCAD
------------

编译说明：（完善中）  

1. 安装最新版本的**[HomeBrew](http://brew.sh/)**.  

2. 通过HomeBrew安装**CMake**, 控制台敲入:  

	```
	brew install cmake
	```

3. 安装最新版本的[wxWidgets 3.0.2](https://github.com/wxWidgets/wxWidgets/releases/download/v3.0.2/wxWidgets-3.0.2.tar.bz2)。  解压缩之后，在wxWidgets的根目录下，使用下面的参数进行配置编译:  

	```
	./configure --with-opengl \  
		--enable-aui \
		--enable-utf8 \
		--enable-html \
		--enable-stl \
		--with-libjpeg=builtin \
		--with-libpng=builtin \
		--with-regex=builtin \
		--with-libtiff=builtin \
		--with-zlib=builtin \
		--with-expat=builtin \
		--without-liblzma \
		--with-macosx-version-min=<osxtarget> \
		--enable-universal-binary=i386,x86_64
	```

	配置编译参数完成之后，编译：  

	```
	make  
	make install
	```

	安装wxWidgets 3.0.2完成之后，可以通过敲入命令行`wx-config`来判断wxWidgets是否安装成功。  

4. 通过HomeBrew安装**OpenSSL**, 控制台敲入: `brew install openssl`。 
 
5. 通过HomeBrew安装**GLEW**, 控制台敲入: `brew install glew`。  

6. 通过HomeBrew安装**cairo**, 控制台敲入: `brew install cairo`。  

7. 通过HomeBrew安装**boost**, 控制台敲入: `brew install boost`。  

8. 控制台进入`kicad`的主目录，敲入:  

	```
	mkdir xcode 	
	cd xcode  
	```
	
	使用**CMake**生成**Xcode**工程，敲入:  
	
	```
	export OPENSSL_ROOT_DIR=/usr/local/opt/openssl  
	cmake -G Xcode .. -DCMAKE_OSX_DEPLOYMENT_TARGET=<osxtarget>`  # 注意：这里的osxtarget最好和之前的wxWidgets的版本保持一致。
	```
	
	完成之后，就能够在当前目录下生成的**Xcode**工程文件。

----------

（未完待续）

